---
layout: handbook-page-toc
title: "Field Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Welcome to the Field Operations Handbook

“Manage field business processes, systems, architecture, enablement, champion data integrity, provide insights and predictability through analytics”

The term *"sales"* refers to the Sales Team and *"field"* encompasses the Customer Success Team. 

### Key Tenants
**Clarity**: for definitions, processes and events   
**Visibility**: to processes, data and analytics   
**Accountability**: for both Sales/Field Ops to uphold to expectations and SLAs   

### Teams
* [Sales Operations](/handbook/sales/field-operations/sales-operations/)
    *   [Deal Desk](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#welcome-to-the-deal-desk-handbook)
* [Sales Systems](/handbook/sales/field-operations/sales-systems/)
* [Sales Strategy](/handbook/sales/field-operations/sales-strategy/) 
* [Field Enablement](/handbook/sales/field-operations/field-enablement/)
* [Channel Operations](/handbook/sales/field-operations/channel-operations/)
