---
layout: handbook-page-toc
title: "Verify and Release UX"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### CI/CD Direction Content Note

The Verify and Release stages [were recently added to the Ops section](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/42825). Some direction content listed on the page is still shown from the former CI/CD section. Over time it will be [migrating to Ops pages](https://gitlab.com/gitlab-com/Product/-/issues/912).

### Overview

GitLab CI/CD is a powerful tool built into GitLab that allows our customers to apply all the continuous methods (Continuous Integration, Delivery, and Deployment) to their software with no third-party application or integration needed.

The goal of the Verify and Release team is to provide a seamless experience and clear guidance to empower users of all knowledge levels to ship their code as fast as possible without interruptions.
Our design mission is bringing to the forefront simple, clean ways to make GitLab be the tool of choice for deploying where, when, and how users want to.

### Team

See all [Verify and Release UX Team members](https://about.gitlab.com/company/team/?department=verify-and-release-ux-team) and open vacancies.

### Our strategy

Our strategy is all about making sure that even complex delivery flows become an effortless part of everyone's primary way of working. We work closely with Engineering, Product Management, User Research, Technical Writing, and Product Marketing to make sure we have all the support in uncovering user needs and work to solve them together.

| Strategy | Goal |
| ------ | ------ |
| Jobs to be done framework | Work with our PMs to identify the top tasks (in frequency or importance) for our users, based on user research (analytics or qualitative findings). By analyzing how they change depending on factors such as size of company, roles and responsibilities, and personas, we can evaluate their experience trhough an UX Scorecard. |
| [UX Scorecards and recommendations](/handbook/engineering/ux/ux-scorecards/) | Creating an UX Scorecards with associated Recommendations enables us to identify our user's workflows, as well as understand different types of workflows from existing, new, and/or non-users, scope and track the efforts of addressing usability concerns for these workflows, explore specific situational problems we are look to solve now and in future iterations. When a scorecard is complete, we have the information required to collaborate with PMs on grouping fixes into meaningful iterations and prioritizing UX-related issues. |
| [Opportunity canvas](https://about.gitlab.com/handbook/product-development-flow/#opportunity-canvas) |  Collaborate with PM during the validation track build a document to understand the user pain, the business value, and the constraints to a particular problem statement. |
| Stakeholder interviews | Serve as a quick and comprehensive way of taking inventory of our current challenges across the Release stage. The analysis of these will then allow us to understand what user data we should be looking for in order to support the team in addressing those challenges. |
| User and customer interviews |  Gather external understanding from GitLab users and non-users. We use feedback from interviews to inform our personas, understand and develop objectives and goals for features. |

Visit [CI/CD Product Section Direction](/direction/ops/) to read about the product strategy.

#### Our strategic counterparts

We have business goals we are shooting for all the time. To understand how we can measure success in the CI/CD area, we collect insights from our strategic counterparts: [Product Marketing Managers](/handbook/marketing/product-marketing/pmmteam/), [Analyst Relations](/handbook/marketing/product-marketing/analyst-relations/), and [Customer Success](/handbook/customer-success/).

<table>
  <tr>
    <th>Stage group</th>
    <th>PMM (Product Marketing Manager)</th>
    <th>AR (Analyst Relations)</th>
    <th>CS (Customer Success)</th>
  </tr>
  <tr>
    <td>
      Verify:Continuous Integration
    </td>
    <td>
      <%= team_links_from_group(group: 'Continuous Integration Group pmm') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Ops Section AR Stable Counterpart') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Continuous Integration Group CS Stable Counterpart') %>
    </td>
  </tr>
  <tr>
    <td>
      Verify:Runner
    </td>
    <td>
      <%= team_links_from_group(group: 'Runner Group pmm') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Ops Section AR Stable Counterpart') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Runner Group CS Stable Counterpart') %>
    </td>
  </tr>
  <tr>
    <td>
      Verify:Testing
    </td>
    <td>
      <%= team_links_from_group(group: 'Testing Group pmm') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Ops Section AR Stable Counterpart') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Testing Group CS Stable Counterpart') %>
    </td>
  </tr>
  <tr>
    <td>
      Package
    </td>
    <td>
      <%= team_links_from_group(group: 'Package Group pmm') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Ops Section AR Stable Counterpart') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Package Group CS Stable Counterpart') %>
    </td>
  </tr>
  <tr>
    <td>
      Release:Progressive Delivery
    </td>
    <td>
      <%= team_links_from_group(group: 'Progressive Delivery Group pmm') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Ops Section AR Stable Counterpart') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Progressive Delivery Group CS Stable Counterpart') %>
    </td>
  </tr>
  <tr>
    <td>
      Release:Release Management
    </td>
    <td>
      <%= team_links_from_group(group: 'Release Management Group pmm') %>
    </td>
   <td>
      <%= team_links_from_group(group: 'Ops Section AR Stable Counterpart') %>
    </td>
    <td>
      <%= team_links_from_group(group: 'Release Management Group CS Stable Counterpart') %>
    </td>
  </tr>
</table>

### Customer

Our solutions are meant to help organisations scale efficiently and effectively by automating software delivery processes and orchestrating deployments to go production at a low cost. We are focusing on these [roles and personas](/handbook/marketing/product-marketing/roles-personas/#user-personas) to create the best user experience possible.

### UX pages

* [Verify UX](/handbook/engineering/ux/stage-group-ux-strategy/verify/)
* [Release UX](/handbook/engineering/ux/stage-group-ux-strategy/release/)

### How we work

#### Quarterly OKRs

[OKRs](https://en.wikipedia.org/wiki/OKR) stand for Objectives and Key Results and we manage them as quarterly goals. They lay out our plan to execute our strategy and help make sure our goals and how to achieve them are clearly defined and aligned throughout the organization.

Learn more about [Verify and Release UX Team OKRs](https://gitlab.com/gitlab-org/gitlab-design/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX%20CICD%20Team&label_name[]=OKR).

#### User research

Our goal is to stay connected with GitLab users all around the world and gather insight into their behaviors, motivations, and goals when using GitLab.

* [Verify UX research insights](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=devops%3A%3Averify)
* [Release UX research insights](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=devops%3A%3Arelease)

Read more about how we do [UX Research at GitLab](/handbook/engineering/ux/ux-research/).

#### Customer journeys

We see customer journeys as stories about understanding our users, how they behave, and what we can do to improve their trip. We use [UX Scorecards](/handbook/engineering/ux/ux-scorecards/) to perform this analysis and evaluate user experience.

See all [Verify and Release UX Scorecards](https://gitlab.com/gitlab-org/gitlab/issues/197959#note_276290767).

#### Understanding business objectives

We work closely with Product Management to understand business goals by collaboratively answering product foundational questions:

* [Release Product Foundations Document](https://gitlab.com/gitlab-org/gitlab-design/issues/392) (ongoing)
* [Verify Product Foundations Document](https://gitlab.com/gitlab-org/gitlab-design/issues/334) (ongoing)

### Our team meetings

#### Verify & Release Design Review (weekly)

The Verify & Release Design Review meeting goals are to showcase the work that every Product Designer in our team is performing, and collect feedback and ideas from other team members. This session is an excellent platform for identifying dependencies between design work in all Verify and Release stage groups.  

* Watch the [Verify & Release Design Review meetings](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpnb8RDztlfpryAYip1OMwb) on YouTube Unfiltered

#### Verify & Release UX Team Meeting (bi-weekly)

We meet bi-weekly as a team to discuss our work, processes, talk about User Research activities, share knowledge, and raise questions to each other. We are also using session for team retrospectives, as well as sharing useful resources around design and DevOps domains.

* Watch the [Verify & Release UX Team Meeting videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KqkrzZyJrJSEWNyiL_5x7an) on YouTube Unfiltered

#### All Ops Design Review (monthly)

We are currently experimenting with meeting once a month with all the Product Designers in Ops UX (Verify, Release, Package, Configure and Monitor stage groups) for Design Review sessions to discuss the design work we are performing, share knowledge, and manage dependencies.

* Watch the [All Ops Design Review videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KpjUgnNM90Yx0icPNjpAGsB) on YouTube Unfiltered

#### Verify & Release UX Milestone Kick-off (monthly)

The Verify and Release UX team is currently experimenting with a [milestone kickoff](/handbook/engineering/ux/ux-department-workflow/#milestone-kickoff) pilot program that is led by designers. It aims to showcase upcoming work within Verify and Release groups, identify overlapping items, trigger design discussions, and find ways to keep the company informed of our UX work.

* Watch the [Verify & Release UX milestone kickoff videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KqkrzZyJrJSEWNyiL_5x7an) on YouTube Unfiltered
* Follow the [pilot issue](https://gitlab.com/gitlab-org/verify-and-release-ux-team/-/issues/6)

#### Ops Cross-Stage ThinkBIG! (monthly)

The purpose of Ops Cross-Stage ThinkBIG! meeting is to discuss the vision, product roadmap, user research, and design work related to the Cross-Stage Ops experience at GitLab.


* Watch the [Ops Cross-Stage ThinkBIG! videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KqFdx966BWkg9-RwXyBDq1k) on YouTube Unfiltered


