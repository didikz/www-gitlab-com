---
layout: markdown_page
title: "Brandon Lyon's README (Marketing web developer/designer)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Brandon Lyon's README

Hi 👋 my name is Brandon Lyon. I'm a marketing web developer and designer at GitLab.

- [GitLab Handle](https://gitlab.com/brandon_lyon)
- [Team Page](https://about.gitlab.com/company/team/#brandon_lyon)
- [My Website](http://about.brandonmlyon.com/)
- [Twitter](https://twitter.com/brandon_m_lyon)
- [Pinterest](https://www.pinterest.com/designbybrandon/)
- [LinkedIn](https://www.linkedin.com/in/brandonmlyon/)

## Current work

[Brandon is currently working on these projects](https://bit.ly/3eNe8I7).

## About Me

- 🤙 I'm born and raised on the West Coast.
- 🏄 I've travelled the world but California is the only place that feels like home.
- ❤️ I love design and development.
- 📖 I'm a lifelong learner who values knowledge.
- 🍸 Coffee, gin, or wine please. I'm not much of a beer person.
- 🕹️ Video games take up most of my spare time.
- 🥏 Sports aren't my thing but I do disc golf.
- 🌲🐈 I'm a plant and animal person.

### Myers-Briggs Personality type:

🔗[**“The Logician”** (INTP)](https://www.16personalities.com/intp-personality)

### 10 Books On My Bookshelf

1. Are Your Lights On?
2. Graphic Content: True Stories From Top Creatives
3. Humans vs Computers
4. The Oxford Companion to American Food and Drink
5. The Architecture of Happiness
6. When: The Scientific Secrets of Perfect Timing
7. 101 Things I Learned In Architecture School
8. Tao of Jeet Kun Do
9. Elements of Persuasion
10. Blood, Sweat, and Pixels

## How I think

- Clarity of communication is paramount, especially when context is insufficient.
- **Who, what, when, where, and why?** Sometimes how (although I prefer to leave how open-ended).
- The phrase "intent vs impact" is of high importance to me.
- I prefer the frank and direct approach and won't take it personally. Decades of design reviews have hardened me.
- ^ I understand and empathize that not everyone else feels that way.
- Please help me document things! I document everything because I have a good short term memory but a terrible long term memory.
- I prefer planning before a task rather than adapting on the fly. Measure twice cut once.
- Long term thinking is preferable vs. short term.
- I plan projects knowing that chaos will happen and I try to expect the unexpected.
- I'm somewhere in-between introvert and extrovert. People are exhausting but I also prefer hanging out to being alone.
- I value self-sufficiency.
- I operate in a judgement-free zone.
- I want you to feel comfortable around me. Please let me know if there is any way I can improve.
- I like to live in the moment. Don't dwell on the past. Tomorrow might never come. Plan assuming both that it will and it won't.
- My background in architecture adds a unique behavioral psychology element to my designs. You can create a sidewalk but people are still going to walk on the grass instead.

## What I do

Hybrid designer-developers are rare. I'm a self-taught developer since a time before CSS existed. I went to college to become an architect but also studied graphic design. I spend lots of my time reading professional journals to keep up to date on industry trends.

### Design

In the digital realm most of my professional experience has been in conversion-oriented marketing websites but I'm also experienced with the UX of user-generated content, e-commerce, brochure-ware, wearables, voice interaction, and mobile app design. In the physical realm I have professional experience with architecture, booth design, tradeshows, industrial design, apparel, and graphic design.

### Development

I can do full stack development but my specialty is frontend development and design. I can Ruby but I'm mostly familiar with javascript. I understand the concepts of MVC (model view controller) and static site generation but again they're not my specialty.

## Favorite quotes

> If a park ranger warns you about the bears, it ain’t cause he’s trying to keep all the bear hugs for himself.

> A designer knows he has achieved perfection not when there is nothing left to add, but when there is nothing left to take away.

> If you don't have time to do it right, when will you have time to do it over?

> Everybody has a plan until they get punched in the face.

> I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel.

## Working with me

First off, I'm in the Pacific time zone and I'm not a morning person! That said, I'm happy to be accommodate your schedule. We're all in this together.

### Planning

There are only so many hours in a week. If you add something to my sprint, then it's likely that something else has to be removed. Sprints & milestones are used to make plans. Lead time is usually measured in weeks and months, not hours. I understand that it's important to be flexible and pivot when appropriate but it's also difficult and stressful when that's the rule rather than the exception.

### Communication

I tend to start the first day of the week by answering issues and only answer issues that day. If you don't hear from me on an issue until next week, that's why. It helps me focus on accomplishing planned tasks. If you need to contact me urgently then you might want to try Slack. That said, I tend to turn off Slack during the second half of the day. This also helps me focus on work that needs to get done.

## Conclusion

🤘Be excellent to each other.
