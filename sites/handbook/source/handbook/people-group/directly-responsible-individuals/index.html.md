---
layout: handbook-page-toc
title: Directly Responsible Individuals
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is a directly responsible individual?

[Apple coined the term](http://fortune.com/2011/08/25/how-apple-works-inside-the-worlds-biggest-startup/) "directly responsible individual" (DRI) to refer to the one person with whom the buck stopped on any given project. 
The idea is that every project is assigned a DRI who is ultimately held accountable for the success (or failure) of that project.

They likely won't be the only person working on their assigned project, but it's ["up to that person to get it done or find the resources needed."](https://originalfuzz.com/blogs/magazine/83782148-the-directly-responsible-individual)

The DRI might be a manager or team leader, they might even be an executive. 
Or, they may themselves be individually responsible for fulfilling all the needs of their project. 
The selection of a DRI and their specific role will vary based on their own skillset and the requirements of their assigned task. 
What's most important is that they're empowered.
We may [disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree), but we all have to achieve results on every decision while it stands, even when if trying to have it changed.

## Empowering DRIs

It is important to understand that DRIs do not owe anyone an explanation for their decisions. If you force a DRI to explain too much, you'll create incentives to ship projects under the radar. The fear of falling into a perpetual loop of explaining can [derail a DRI](/handbook/values/#five-dysfunctions), and cause people to defer rather than working with a [bias for action](/handbook/values/#bias-for-action).

We would much rather foster a culture where DRIs are willing to put their ideas in the open. This enables feedback from a broad range of diverse perspectives, which the DRI can take into account and choose how (if at all) it shapes their thinking.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/jdN5mj5ieLk?start=1775" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

As part of a Harvard Business School case study [interview](https://youtu.be/jdN5mj5ieLk) (shown above), GitLab co-founder and CEO Sid Sijbrandij spoke with Professor Prithwiraj Choudhury on various elements of GitLab's all-remote structure, including a question on DRIs.

> How do we get the best of consensus organizations? When we're about to make a decision, we [tell everyone](/handbook/communication/#making-a-companywide-announcement). Everyone can give input.
>
> How we keep the best of hierarchical organizations is by having a DRI — one person who will decide.
>
> Now, there's one tricky thing here. If you make the DRI convince or explain their decision too much to all the people who provided input, **you'll get projects that fly under the radar**. I've seen this in a lot of companies.
>
> Very explicitly, a DRI [at GitLab] does not have to explain why they're making a decision, and they absolutely do not have to convince other people. 
>
> So, you get to provide input, but you don't have the right to feeling heard or being considered in the eventual decision. 

## DRIs and our Values

**At the end of the day, it's about [results](/handbook/values/#results) and [efficiency](/handbook/values/#efficiency).** 
DRIs work conceptually because they leave no room for ambiguity about who has the final say on all questions that arise within a project or team.

**Assigning one, ultimately responsible person to a project might seem to impair our ability to [collaborate](/handbook/values/#collaboration) effectively at first glance, but that's misleading.** 
The DRI should be wholly invested in their assignment and welcome collaboration in order to succeed. 
While they're empowered to make all final decisions, they should know how and when to trust in the experience and judgment of their teams and peers.

**Of course, when things do go wrong, it's also the DRI who (usually) takes the fall** as was the case when Scott Forestall, then iOS senior vice president, was forced to resign after he ["refused to sign the letter apologizing"](http://fortune.com/2012/10/29/inside-apples-major-shakeup/) for Apple's infamously error-laden Maps app redesign in 2011.

## Characteristics of a Project DRI

DRIs are most often assigned at the task-level. For example, when building a new product feature [the Product Manager is the DRI for the prioritization and the Engineering Manager is the DRI for delivery](https://about.gitlab.com/handbook/product/product-management/process/#working-with-your-group). As [managers of one](https://about.gitlab.com/handbook/values/#managers-of-one) GitLab team members are most often the DRI for the tasks they accomplish. 


At times, someone may be a DRI for an entire process or project. Because this comes with additional responsibility, there are some suggested characteristics to keep in mind when assigning the DRI. 
[Mike Brown](http://brainzooming.com/about-brainzooming/mike-brown/) in his November 2015 article, [Project Management – 8 Characteristics of a DRI](http://brainzooming.com/project-management-8-chracteristics-of-a-dri/25340/), lists the following:

1. Detail-orientated without ever losing a strong strategic perspective.
1. Calm under the pressure of implementation and deadlines.
1. A strong listener with great skill at asking questions.
1. Able to vary the direction of project (or tactic or task) in smart ways to keep moving toward the objective.
1. Adept at anticipating potential problems and addressing them early.
1. Able to successfully interact at senior and junior levels within the organization.
1. Resilient in order to recover from setbacks.
1. Consistent in how they respond to comparable situations.

The DRI is also part of a team, a team needs to be motivated and aligned on achieving the steps to get to success. 
The DRI will also be responsible for making sure the team gets there.

## Communication and feedback

A DRI should be able to articulate the objectives, check progress and give and receive feedback. 
This will ensure the DRI can change direction or plan ahead to avoid any setbacks.

At GitLab we communicate and work [asynchronously](/company/culture/all-remote/management/#asynchronous), you can read more about it on [this page](/handbook/communication/).

One thing to consider when a DRI needs to give or receive feedback is that they may not be the actual manager of the other members of the team. 

Giving or receiving feedback is tough and we have looked at this in our previous [Guidance on Feedback Training](/handbook/people-group/guidance-on-feedback/). See also GitLab's [guide to communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/). 

## RADCIE

Different organizations use different methods of assigning responsibility; 
one of the most popular is the [RACI Matrix](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix), which outlines who the Responsible-Accountable-Consulted-Informed folks should be on a decision or project. 

GitLab's implementation of a DRI for decision-making means that we have evolved the RACI matrix to RADCIE. 
The **Responsible** and **Accountable** person is the **DRI**, and given that **[Everyone Can Contribute](/company/strategy/#mission)**, the **Consulted** and **Informed** people are **Everyone**.

There are extenuating circumstances where we do approvals for coordination, but they are extremely rare, and it is the responsibility of the DRI to recognize the need and continue to move the project forward. 

## Further reading

1. [How well does Apple's DRI model work in practice](https://www.forbes.com/sites/quora/2012/10/02/how-well-does-apples-directly-responsible-individual-dri-model-work-in-practice/#4d83402d194c)
1. [Matthew Mamet, DRI](https://medium.com/@mmamet/directly-responsible-individuals-f5009f465da4)
1. [Communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/)
1. [GitLab Handbook, Guidance on Feedback](/handbook/people-group/guidance-on-feedback/)
