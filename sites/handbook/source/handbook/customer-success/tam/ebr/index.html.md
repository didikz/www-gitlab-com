---
layout: handbook-page-toc
title: "Executive Business Reviews (EBRs)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## What is an Executive Business Review?

An Executive Business Review (EBR) is a strategic meeting with stakeholders from both GitLab and the customer. TAMs are responsible for scheduling and conducting EBRs and working with their customers to achieve the primary objectives.

TAMs should hold EBRs with each of their customers **at least** once per year, but more frequently is better to ensure the customer continues to engage and see the value of GitLab; however, this can vary depending on the needs of the customer.

The purpose of the EBR is to demonstrate to the [Economic Buyer](https://about.gitlab.com/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification) the value they are getting out of GitLab. It is meant to be interactive from both sides, discussing the usage of GitLab, how the customer has been using it, how it aligns with their business goals, and more. EBRs should not be treated as a sales call or an opportunity to up-sell, but rather to show that GitLab was a worthwhile investment and how we are working together as partners.

EBRs typically consist of the following content, but this list is only a guideline and should be tailored to the needs of each customer.

- Introductions
- Overview of GitLab & our Customer Success
- Product Roadmap
- Year in Review (growth, usage, etc.)
- Past & Future Business Goals
- Support Review
- Delivered Enhancements
- Relationship Building
- Q&A

## How to prepare an EBR

Please view our [EBR Playbook](https://docs.google.com/spreadsheets/d/1nGjXMaeAFWEOGdsm2DPW-yZEIelG4sy46pX9PbX4a78/edit#gid=0) (internal to GitLab) for more details on how to propose, prepare, and present an EBR. This also includes a link to an [EBR sell sheet](https://docs.google.com/document/d/1jHdzEES5R2wEo6qEXeDCoDeLU1Ad41tULpJpEm3kUmQ/edit) which TAMs can copy and edit to send to their customers to help demonstrate what the customer will get out of the EBR, as well as an "[EBR in a Box](https://docs.google.com/presentation/d/1V3wzIZ9j6pVUbXpSeJgA_Lk-97C7_vr8NUJWk4J0__s/edit?usp=sharing)" presentation which contains several pointers on the logistics of preparing, such as a suggested timeline, how to prepare, and tips on presenting.

There are also several example EBR decks and past recordings linked in the playbook and EBR in a Box for TAMs and other GitLabbers to review and take inspiration from (please keep all customer-specific content confidential).
