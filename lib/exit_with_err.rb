module ExitWithErr
  def exit_with_err(status = 1)
    # Attempt to ensure that the output is always successfully captured by the CI job log.
    # There have been instances where directly calling `exit` results in the
    # output not being captured by the CI/CD job log
    # (see https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8142).
    # This ensures that the Ruby output buffers are flushed, and then sleeps to
    # allow any buffering above the Ruby level to also flush its output - for example,
    # buffering at the OS or virtualization level.
    #
    # Note that this should only be needed in code which is run via CI/CD jobs.
    # This should not be necessary for code which is only run on a local machine.
    STDOUT.flush
    STDERR.flush
    sleep 1
    exit status
  end
end
