---
layout: markdown_page
title: "Category Direction - Live Preview"
---

- TOC
{:toc}

## Live Preview

| | |
| --- | --- |
| Stage | [Create](/direction/dev/#create) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-06-24` |

### Introduction and how you can help
Thanks for visiting this direction page on the [Live Preview](https://docs.gitlab.com/ee/user/project/web_ide/index.html#enabling-client-side-evaluation) features of the [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/index.html). This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by Kai Armstrong ([E-Mail](mailto:karmstrong@gitlab.com)).

Learn more about the [Web IDE direction](/direction/create/web_ide/).

### Overview

With Live Preview can view your simple JavaScript apps and static sites in the Web IDE, in real time, right next to the code.

We're not currently investing in the Live Preview features of the Web IDE, but welcome contributions to improve the existing feature or extend our Live Preview capabilities to full stack applications.

### What is Not Planned Right Now

Live Preview is currently limited to Javascript applications that can be evaluated in the browser, called client-side evaluation. Extending support to include server-side evalaution would allow more complex applications to be previewed in real time through the Web IDE. Currently this is not possible, and is not planned.
