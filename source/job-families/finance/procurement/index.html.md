---
layout: job_family_page
title: "Procurement"
---

## Procurement Operations Analyst

The Procurement Operations Analyst supports procurement and spend management activities for all spend categories.
Theywork cross-functionally to analyze spend data, research suppliers, and support the preparation and analysis of strategic sourcing events.

### Job Grade

The Procurement Operations Analyst is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).


### Responsibilities

* Support the Procurement Team and internal stakeholders for procurement and sourcing related initiatives.
* Maintain and update the procurement process to meet the business needs in a rapid manner.
* Support the business stakeholders with requisition and process questions or challenges.
* Conduct analysis in various tools to support procurement activities including spend analysis, budget analysis, bid analysis, etc.
* Leverage spend analytics to identify consolidation opportunities, understand total cost of ownership, and rationalize opportunities.
* Update and leverage data within multiple enterprise software solutions to support business objectives.
* Analyze market intelligence to build business cases and cost models to identify and quantify improvement opportunities for demand management, process improvements, build vs. buy, etc.
* Develop and maintain procurement metrics (Performance Indicators, benchmarking & SLAs) with collaboration of procurement and internal stakeholder teams.
* Provide procurement reporting on a recurring basis.

### Requirements

* Bachelor's degree in relevant field
* Ability to navigate cultural differences and build global but locally relevant solutions
* Strong social and communication skills (verbal and written), across all levels
* Excellent organizational, time management, and priority setting skills
* Deadline oriented; able to work in a fast-paced environment with ever-changing priorities
* Self-motivated with the ability to work both independently and collaboratively
* Proficient in Google Docs
* You share our values, and work in accordance with those values
* Successful completion of a background check
* Ability to use GitLab

## Performance Indicators

* [Percent of Vendor Spend on Purchase Order](/handbook/business-ops/metrics/#percent--of-vendor-spend-on-purchase-order)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below.
Please keep in mind that candidates can be declined from the position at any stage of the process.
To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute screening call with a member of our recruiting team
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Procurement Manager
* After that, candidates will be invited to schedule a 30 minute interview with a Business Systems Analyst
* Next, the candidate will be invited to interview with a Finance Business Partner
* Next, candidates will be invited to schedule a 30 minute interview with our Director of Business Operations
* Finally, our CEO may choose to conduct a final interview

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Manager, Procurement

### Job Grade

The Manager, Procurement is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

GitLab is looking for a professional with Source to Pay experience within a global company. This is a new function and we expect you to take on the existing workload as an IC, making immediate iterative improvements to existing process, while building the team and function for the long term.  The ideal candidate will have a passion for designing, optimizing processes and systems, and has a strong focus on customer service, teamwork, and collaboration. We expect you to demonstrate the ability to work in a fast paced, both individually and as a part of a group, and thrive within a dynamic and rapidly changing environment.

### Responsibilities

- Develop and manage GitLabs procurement program by implementing low-bureaucracy, efficient new policies, procedures and systems for best purchasing practices
- Control spend and build a culture of long-term saving on procurement costs
- Build strong relationships with internal and external stakeholders to negotiate and finalize terms and conditions on contracts
- Manage contracts renewals, month over month usage, and vendor due diligence
- Manage the execution of request for proposals, bid analysis, contract development, and negotiation, ensuring that the end to end process is well documented and transparent.
- Evaluate and process purchase requisitions efficiently to reduce downstream holds and positively impact payment cycles
- Drive process improvement and continuous improvement in all aspects of assigned job duties
- Work closely with Accounts Payable and internal business partners to promptly resolve supplier payment issues
- Facilitate supplier selection and develop data analysis and requirements for informed supplier decisions
- Serve as subject matter expert for best-in-class procurement practices and tools, and experienced in working with numerous cross-functional disciplines
- Work with operations teams to identify and execute new or creative approaches, strategies and processes to address a business need
- Develop policies and procedures and lead negotiations with key business partners

### Requirements

- At least 3-5 years of experience in procurement at a technology company
- BA/BS in Supply Chain, Business, Finance, or other related field; MBA preferred
- Experience in contract analysis, negotiation, financial modelling and RFP/ RFI/RFQ creation and evaluation.
- Knowledge and experience negotiating in support of the following functions Marketing, Sales, IT, and Professional Services
- Experience with acquisition integration in a Sourcing Role is a plus
- Candidate should be able to effectively build relationships as well as influence and drive change in a cross-functional team environment
- Top-notch communication, analytical and organization skills, including experience manipulating data to derive insights for senior management or executives
- Self-starter with ability to own projects from initial inquiry to completion
- Ability to use GitLab

## Senior Manager, Procurement

The Senior Manager, Procurement  role extends the [Procurement Manager](#requirements) role.

### Job Grade

The Senior Manager, Procurement is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* All requirements of an Intermediate Procurement Manager
* Understand and implement best practices
* Build a _great_ procurement program at GitLab
* Drive new and creative approaches, strategies and processes to address business needs, drawing on relevant first hand experience for what works
* Generate sourcing and procurement process improvement recommendations across functions and implement them
* Great communication: Regularly achieve consensus amongst teams

## Performance Indicators (PI)

*  [Percent of vendor spend on Purchase Order](/handbook/business-ops/metrics/#percent--of-vendor-spend-on-purchase-order)
*  [Vendor cost avoidance](/handbook/business-ops/metrics/#vendor-cost-avoidance)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their title on our team page.

- Selected candidates will be invited to schedule a screening call with a member of our Recruiting team
- Next, candidates will be invited to schedule a first interview with our VP of Legal
- Next, candidates will be invited to schedule an interview with our Controller
- Next, candidates will be invited to schedule an interview with our Director of Business Operations
- Next, candidates will be invited to schedule an interview with our Chief Financial Officer
- Finally, candidates may be asked to interview with our CEO.

Additional details about our process can be found on our [hiring page](/handbook/hiring)

## Career Ladder

The next step in the Procurement job family is to move to TODO

