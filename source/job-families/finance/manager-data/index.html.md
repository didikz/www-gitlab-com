---
layout: job_family_page
title: "Manager, Data"
---

## Responsibilities

* Manage our data warehouse so it can support data analysis and operational requirements from all functional groups
* Work with Product to operate and analyze a unified data ecosystem that includes our SaaS, Telemetry, and other usage data
* Be the data expert supporting cross-functional teams, gathering data from various sources, and enabling automated reporting to democratize data across the company
* Hold regular 1:1’s with all members of the Data Team
* Triage and manage development priorities of analytics dashboards and data pipelines
* Represent the Data Team in different company functions - be an advocate for holistic dataflow systems thinking
* Create and execute a plan to develop and mature our ability to measure and optimize usage growth and our user journey
* Regularly give Data group conversations and participate in Monthly KPI meetings
* Implement the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* Collaborate with all functions of the company to ensure data needs are addressed
* Build upon and document our common data framework so that all data can be connected and analyzed
* This position reports to the Director of Business Operations


## Requirements

* 2+ years hands on experience in a data analytics/engineering/science role
* 2+ years managing a team of 2 or more data analysts/engineers/scientists
* Experience working with leadership to define and measure KPIs and other operating metrics
* Experience growing a team in a fast-paced, high-growth environment
* Demonstrably deep understanding of SQL and relational databases (we use Snowflake)
* Ability to reason holistically about end-to-end data systems: from ETL to Analysis to Reporting
* Hands on experience working with Python
* Experience building and maintaining data pipelines (Airflow preferred)
* Experience building reports and dashboards in a data visualization tool (we use Periscope)
* Experience with open source data warehouse tools
* Be passionate about data, analytics, and automation, especially in applying software engineering principles to data science and analytics
* Experience working with large quantities of raw, disorganized data
* Experience with Salesforce, Zuora, Zendesk and Marketo
* Strong written and verbal communication skills
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab


## Performance Indicators (PI)

*  [New Hire Location Factor < 0.69](/handbook/business-ops/metrics/#new-hire-location-factor--069)
*  [% of team who self-classify as diverse](/handbook/business-ops/metrics/#percent--of-team-who-self-classify-as-diverse)
*  [Discretionary bonus per employee per month > 0.1](/handbook/business-ops/metrics/#discretionary-bonus-per-employee-per-month--01)
*  [Number of new trainings hosted per month > 2](/handbook/business-ops/metrics/#number-of-new-trainings-hosted-per-month--2)


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Director of Business Operations
* Next, candidates will be invited to schedule a second interview with our Finance Operations and Planning Lead
* Next, candidates will be invited to schedule one or more interviews with members of the BizOps team
* Finally, candidates may be asked to interview with our CFO or CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
