!function(){

  var loopcount;
  var attemptedAction;
  var extraClassnames = "";

  function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : "";
  }

  if(window.CBNCLanguage === undefined)
  {
    window.CBNCLanguage = "en";
  };

  var renderMessageStandard = function(loopcount, targets, attemptedAction, extraClassnames, messageLocation, isThisExpandable)
  {
    if(window.CBNCLanguage.toLowerCase() == "de")
    {
      var CBNCMessage  = '<div class="cbnc-message' + extraClassnames + ' cbnc-message-expands-' + isThisExpandable + '"><span class="cbnc-message-toggle">Haben Sie Probleme ' + attemptedAction + '?</span> <span class="cbnc-message-expandable">Möglicherweise müssen Sie Ihre <a href="javascript:Cookiebot.renew();">Cookie-Einstellungen aktualisieren, um Personalisierungs-Cookies zuzulassen</a></span></div>';
    } else {
      var CBNCMessage  = '<div class="cbnc-message' + extraClassnames + ' cbnc-message-expands-' + isThisExpandable + '"><span class="cbnc-message-toggle">Having trouble ' + attemptedAction + '?</span> <span class="cbnc-message-expandable">You may need to update your <a href="javascript:Cookiebot.renew();">cookie settings to allow personalization (personal information) cookies.</a></span></div>';
    };
    var wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'cbneedsconsent');
    targets[loopcount].parentNode.insertBefore(wrapper, targets[loopcount]);
    wrapper.appendChild(targets[loopcount]);
    if(messageLocation == "before") 
    {
      wrapper.insertAdjacentHTML('afterbegin', CBNCMessage);
    };
    if(messageLocation == "after") 
    {
      wrapper.insertAdjacentHTML('beforeend', CBNCMessage);
    };
    if(messageLocation == "afterSubtitle") 
    {
      wrapper.querySelector('.form-container .form').insertAdjacentHTML('beforebegin', CBNCMessage);
    };
    if(isThisExpandable === true) 
    {
      expandableTargets = document.querySelectorAll('.cbnc-message-expands-true');
      for(loopcount=0;loopcount<expandableTargets.length;loopcount++)
      {
        expandableTargets[loopcount].addEventListener('click', function()
        {
          var expandThis = document.querySelectorAll('.cbnc-message-expandable');
          for(loopcount2=0;loopcount2<expandThis.length;loopcount2++)
          {
            expandThis[loopcount2].style.display = "inline";
          };
        });
      };
    };
  }

  isCookieConsentRequired = getCookie("CookieConsent");
  if (isCookieConsentRequired !== "-1")
  /* if consent is required */
  {
    if (isCookieConsentRequired.indexOf("marketing:true") == -1)
    /* if personalization is not currently enabled */
    {
      // site header search
      var targets=document.querySelectorAll('header .search-box');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        attemptedAction = "using search";
        extraClassnames = " cbnc-search-header";
        messageLocation = "after";
        isThisExpandable = false;
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation, isThisExpandable);
      };
      // handbook search
      var targets=document.querySelectorAll('#search-handbook');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        attemptedAction = "using search";
        extraClassnames = " cbnc-search-handbook";
        messageLocation = "before";
        isThisExpandable = false;
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation, isThisExpandable);
      };
      // youtube videos
      var targets=document.querySelectorAll('iframe[data-cookieblock-src*="youtube"]');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        var dataSrc = targets[loopcount].getAttribute('data-cookieblock-src');
        attemptedAction = "<a href='" + dataSrc + "' target='_blank'>viewing this video</a>";
        extraClassnames = "";
        messageLocation = "before";
        isThisExpandable = false;
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation, isThisExpandable);
      };
      // google calendars
      var targets=document.querySelectorAll('iframe[data-cookieblock-src*="calendar"]');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        var dataSrc = targets[loopcount].getAttribute('data-cookieblock-src');
        attemptedAction = "<a href='" + dataSrc + "' target='_blank'>viewing this calendar</a>";
        extraClassnames = "";
        messageLocation = "before";
        isThisExpandable = false;
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation, isThisExpandable);
      };
      // sched events
      var targets=document.querySelectorAll('iframe[data-cookieblock-src*="sched"]');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        var dataSrc = targets[loopcount].getAttribute('data-cookieblock-src');
        attemptedAction = "<a href='" + dataSrc + "' target='_blank'>viewing this schedule</a>";
        extraClassnames = "";
        messageLocation = "before";
        isThisExpandable = false;
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation, isThisExpandable);
      };
      // marketo gated content forms
      var targets=document.querySelectorAll('.form-to-resource-content .form-container, .sales form, .bl-modal form, .centered-form form, .demo-form-container form, .newsletter-form-content form, .newsletter-form-short form');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        if(window.CBNCLanguage.toLowerCase() == "de")
        {
          attemptedAction = "beim Absenden dieses Formulars";
        } else {
          attemptedAction = "with this form";
        };
        extraClassnames = "";
        var formSubtitleExists = targets[loopcount].querySelector('.form-container .f2r-cta-subtitle');
        if(typeof(formSubtitleExists) != 'undefined' && formSubtitleExists) 
        {
          messageLocation = "afterSubtitle";
        }
        else {
          messageLocation = "after";
        };
        isThisExpandable = true;
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation, isThisExpandable);
      };
    }
  };

  // force cookiebot to refresh the page after someone updates their cookie preferences per #7845
  window.addEventListener('CookiebotOnAccept', function (e)
  {
    if (Cookiebot.changed)
    {
       document.location.reload();
    }
  });

  // because cookiebot intercepts the load event, we have to start it again to fix other scripts...
  //dispatchEvent(new Event('load'));
  // commenting out for now to test a theory related to https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8052

}();